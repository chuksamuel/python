test_string = "Global scope."

def test_global_scope():
    test_string = "Local scope."
    print("Executed from function: " + test_string)


test_global_scope()
print("Executed globally: " + test_string)
